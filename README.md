# Object Detection Python

## Setup

### Setting up virtual env (optional)

```
$ python3 -m venv venv -- prompt benchmark_python
$ source venv/bin/activate
```

### Installing requireg python packages

```
pip3 install -r requirements.txt

```

### Downloading benchmark data

```
$ python3 setup.py
```

# Running

```
usage: main.py [-h] [-m MODEL] [-i ITERATIONS]

optional arguments:
  -h, --help            show this help message and exit
  -i ITERATIONS, --iterations ITERATIONS
                        number of performed tests
```
