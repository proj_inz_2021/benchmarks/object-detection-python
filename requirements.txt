gdown==4.2.0
numpy==1.19.5
Pillow==8.4.0
tensorflow==2.4.1
tflite_runtime==2.5.0
