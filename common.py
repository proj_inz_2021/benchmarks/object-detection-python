
# Paths
DATASET_FOLDER = "dataset/"
MODELS_FOLDER = "models/"
OUTPUT_FOLDER = "output/"
DELAY_FILE_NAME = "delays.json"
USE_TPU = True

# Default arguments
MODEL_NAME = 'modelV3LargeCoco.tflite'
TEST_ITERATIONS = 1
