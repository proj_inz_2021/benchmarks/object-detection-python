import os
import importlib.util
import json
import platform
import zipfile
from pathlib import Path
from typing import List, Dict, Union
from PIL import Image

from common import DATASET_FOLDER, MODELS_FOLDER, OUTPUT_FOLDER, DELAY_FILE_NAME, USE_TPU

pkg = importlib.util.find_spec('tflite_runtime')
if pkg:
    from tflite_runtime.interpreter import Interpreter
    if USE_TPU:
        from tflite_runtime.interpreter import load_delegate
else:
    from tensorflow.lite.python.interpreter import Interpreter
    if USE_TPU:
        from tensorflow.lite.python.interpreter import load_delegate


def save_model_output(model_name: str, test_number: int, image_name: str, output_data: str) -> None:
    output_dir = os.path.join(get_output_dir(model_name, test_number), "boxes")
    Path(output_dir).mkdir(parents=True, exist_ok=True)
    image_path = os.path.join(output_dir, image_name.replace('jpg', 'txt'))
    with open(image_path, 'w') as f:
        f.write(output_data)


def save_delay(model_name: str, test_number: int, delays: List[Dict[str, Union[str, float]]]) -> None:
    output_dir = get_output_dir(model_name, test_number)
    delays_path = os.path.join(output_dir, DELAY_FILE_NAME)
    with open(delays_path, 'w') as f:
        json.dump(delays, f, indent=4)


def get_output_dir(model_name: str, test_number: int):
    device_dir = os.path.join(OUTPUT_FOLDER, get_device_name())
    model_dir = os.path.join(device_dir, model_name)
    test_dir = os.path.join(model_dir, "test_%s" % test_number)
    Path(test_dir).mkdir(parents=True, exist_ok=True)
    return test_dir


def get_device_name():
    device_info = platform.uname()
    return '_'.join([device_info.node, device_info.system, device_info.version]).replace(' ', '-')


def get_images_names() -> List[str]:
    return os.listdir(DATASET_FOLDER)


def get_image(image_name: str) -> Image:
    image_path = os.path.join(DATASET_FOLDER, image_name)
    return Image.open(image_path)


def get_model_names() -> List[str]:
    return os.listdir(MODELS_FOLDER)


def load_model(model_name: str):
    model_path = os.path.join(MODELS_FOLDER, model_name)
    if USE_TPU:
        interpreter = Interpreter(model_path=model_path, experimental_delegates=[load_delegate('libedgetpu.so.1.0')])
    else:
        interpreter = Interpreter(model_path=model_path)
    interpreter.allocate_tensors()
    return interpreter


def zip_output_dir():
    zip_path = os.path.join(OUTPUT_FOLDER, "%s.zip" % get_device_name())
    zip_f = zipfile.ZipFile(zip_path, 'w', zipfile.ZIP_DEFLATED)
    for root, dirs, files in os.walk(OUTPUT_FOLDER):
        for file in files:
            zip_f.write(os.path.join(root, file), os.path.relpath(os.path.join(root, file),
                                                                  os.path.join(OUTPUT_FOLDER, '..')))
    zip_f.close()
