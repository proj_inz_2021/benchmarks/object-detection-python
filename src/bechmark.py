import time
from typing import Dict, List, Tuple

import numpy as np

from labels.coco_labels import category_index
from .file_support import load_model, get_image, get_images_names, save_model_output, save_delay, zip_output_dir


class Benchmark:
    def __init__(self, model_name: str):
        self.model = load_model(model_name)
        self._model_name = model_name
        self.sorted_output_details: List[Dict] = sorted(self.model.get_output_details(), key=lambda x: x['index'])

    def run_test(self, no_iterations: int):
        input_details = self.model.get_input_details()[0]

        # Get preprocessed images
        t0 = time.time()
        images = self._prepare_images()
        no_images = len(images)
        print("Test images converted in %ss" % round(time.time() - t0, 5))

        for no_test in range(no_iterations):
            delays = []

            for j, image in enumerate(images):
                image_name, input_data, image_size = image
                self.model.set_tensor(input_details['index'], input_data)

                # Measure activation time
                t0 = time.time()
                self.model.invoke()
                delay = round((time.time() - t0) * 1000)
                delays.append({
                    'image': image_name,
                    'time': delay
                })

                if 'mobilenet' in self._model_name and 'quant' in self._model_name:
                    number_of_classes = self.get_output_tensor(2).round()
                    detection_boxes = self.get_output_tensor(1)
                    detection_classes = self.get_output_tensor(3).round().astype(np.uint32) + 1
                    detection_scores = self.get_output_tensor(0)
                else:
                    number_of_classes = self.get_output_tensor(3).round()
                    detection_boxes = self.get_output_tensor(0)
                    detection_classes = self.get_output_tensor(1).round().astype(np.uint32) + 1
                    detection_scores = self.get_output_tensor(2)

                if number_of_classes != len(detection_classes):
                    number_of_classes = len(detection_classes)

                assert detection_boxes.shape == (number_of_classes, 4)
                assert detection_classes.shape == (number_of_classes,)
                assert detection_classes.dtype == np.uint32
                assert detection_scores.shape == (number_of_classes,)
                assert detection_scores.dtype in [np.float32, np.float64] and 0 <= max(detection_scores) <= 1

                output_data = {
                    'boxes': detection_boxes.tolist(),
                    'classes': detection_classes.tolist(),
                    'scores': detection_scores.tolist()
                }

                # Save result
                t0 = time.time()
                save_model_output(self._model_name, no_test, image_name, self._parse_to_txt(output_data, image_size))
                save_time = round((time.time() - t0) * 1000)

                print("\r %s test %s/%s, image %s/%s, activation=%sms save=%sms" %
                      (self._model_name, no_test + 1, no_iterations, j + 1, no_images, delay, save_time), end='')

            save_delay(self._model_name, no_test, delays)
        print()
        zip_output_dir()

    def get_output_tensor(self, index) -> np.ndarray:
        if self.sorted_output_details[index]['dtype'] == np.float32:
            return self.model.get_tensor(self.sorted_output_details[index]['index'])[0]
        elif self.sorted_output_details[index]['dtype'] == np.uint8:
            scale, zero_point = self.sorted_output_details[index]['quantization']
            output = self.model.get_tensor(self.sorted_output_details[index]['index'])[0]
            return (output - zero_point) * scale
        else:
            raise ValueError("Output details in improper type")

    def _prepare_images(self) -> List[Tuple[str, np.ndarray, Tuple[int, int]]]:
        result_images = []

        input_details = self.model.get_input_details()[0]
        image_shape = (input_details['shape'][2], input_details['shape'][1])

        for image_name in get_images_names():
            base_img = get_image(image_name)

            resized_img = base_img.resize(image_shape)
            input_data = np.expand_dims(resized_img, axis=0)

            if input_details['dtype'] == np.float32:
                input_data = (np.float32(input_data) - 127.5) / 127.5

            result_images.append((image_name, np.array(input_data), base_img.size))
        return result_images

    @staticmethod
    def _parse_to_txt(output_data: Dict[str, List], img_size: Tuple[int, int]) -> str:
        assert all([key in output_data.keys() for key in ['boxes', 'classes', 'scores']])

        results = []
        for box, cls, score in zip(*[output_data[key] for key in ['boxes', 'classes', 'scores']]):
            resized_boxes = [int(box[1] * img_size[0]), int(box[0] * img_size[1]),
                             int(box[3] * img_size[0]), int(box[2] * img_size[1])]
            class_name = category_index[cls]['name'] if cls in category_index else 'None'

            row = "%s %s %s %s %s %s" % (class_name.replace(' ', '_'), score, *resized_boxes)
            results.append(row)
        return '\n'.join(results)
